package main

import (
	"fmt"
	"os"

	"github.com/imroc/req"
)

// UploadFile : Function to upload a single file
func UploadFile(filePath, fileName, remoteDir, baseURL, password string) (string, error) {

	file, err := os.Open(filePath)

	if err != nil {
		return "N/A", err
	}

	uploadURL := fmt.Sprintf("%s/capture/save/resource", baseURL)
	header := req.Header{
		"Cookie":  fmt.Sprintf("passcode=%s", password),
		"Origin":  baseURL,
		"Referer": fmt.Sprintf("%s/capture/section", baseURL),
	}
	param := req.Param{
		"keepfilename": true,
		"folderpath":   remoteDir,
	}
	fileUpload := req.FileUpload{
		FieldName: "File",
		FileName:  fileName,
		File:      file,
	}

	resp, err := req.Post(uploadURL, header, param, fileUpload)

	if resp == nil {
		return "N/A", err
	}

	return resp.Response().Status, err
}
