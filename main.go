package main

import (
	"log"
	"path/filepath"
	"sync"
	"time"

	"github.com/fatih/color"
	"gopkg.in/alecthomas/kingpin.v2"
)

var (
	baseURL      = kingpin.Flag("url", "Base Url (full path including protocal)").Short('u').Required().String()
	dir          = kingpin.Arg("dir", "Directory To Upload").Default("./").String()
	remoteDirURL = kingpin.Flag("remote", "Remote Dir").Short('r').Default("").String()
	password     = kingpin.Flag("password", "Password to qoom site").Short('p').Required().String()
)

func main() {
	kingpin.Parse()
	c := FindFiles(*dir)
	wg := new(sync.WaitGroup)
	for file := range c {
		go func(file FileWithDir) {
			wg.Add(1)
			remoteFolder := filepath.Clean(*remoteDirURL+file.fileDir+"/") + "/"
			status, err := UploadFile(file.filePath, file.fileName, remoteFolder, *baseURL, *password)
			statusColor := color.New(color.FgRed)
			if status[:3] == "200" {
				statusColor = color.New(color.FgGreen)
			}
			errStr := ""
			if err != nil {
				errStr = color.RedString(" - %s", color.RedString(err.Error()))
			}

			log.Printf(
				"%s - %s - %s%s\n",
				color.BlueString(file.fileName),
				color.YellowString(remoteFolder),
				statusColor.Sprint(status),
				errStr,
			)

			wg.Done()
		}(file)
		time.Sleep(10 * time.Millisecond)
	}
	wg.Wait()
}
