# Qoom Upload

## Usage

```
usage: qoom-upload --url=URL --password=PASSWORD [<flags>] [<dir>]

Flags:
      --help               Show context-sensitive help (also try --help-long and --help-man).
  -u, --url=URL            Base Url (full path including protocal)
  -r, --remote=""          Remote Dir
  -p, --password=PASSWORD  Password to qoom site

Args:
  [<dir>]  Directory To Upload
```

ex:

Upload contents from ./ to root directory on qoom

```sh
qoom-upload -u https://www.tongmiao.cloud -p password
```

Upload contents from '/home/person1/thing-folder' to '/thing/' on qoom

```sh
qoom-upload -u https://www.tongmiao.cloud -p password -r /thing/ /home/person1/thing-folder
```

## Install

1) grab the executable for your os and arch from [releases](https://gitlab.com/illemonati/qoom-upload/-/releases)

or

2) if you have go installed, run ```go get gitlab.com/illemonati/qoom-upload```
