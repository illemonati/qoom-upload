package main

import (
	"os"
	"path/filepath"
)

// FileWithDir : A struct that contains a file with its relative dir
type FileWithDir struct {
	filePath string
	fileDir  string
	fileName string
}

// FindFiles : Function to find all files in Directory
func FindFiles(path string) chan FileWithDir {
	c := make(chan FileWithDir)
	go func() {
		findFiles(c, path)
		close(c)
	}()
	return c
}

func findFiles(c chan FileWithDir, path string) {
	filepath.Walk(path, func(filePath string, info os.FileInfo, err error) error {
		if path == filePath {
			return nil
		}

		if info.IsDir() {
			return nil
		}

		relPath, _ := filepath.Rel(path, filePath)
		relDir := filepath.Dir(relPath)
		fileName := filepath.Base(filePath)

		c <- FileWithDir{
			filePath: filePath,
			fileDir:  relDir,
			fileName: fileName,
		}
		return nil
	})
}
